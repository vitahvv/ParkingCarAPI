﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ParkingCar.Model
{
   public class Car
    {    [Key]
         [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCar { get; set; }
        public int Balance { get; set; }
        public CarType CType { get; set; }

    }
}
