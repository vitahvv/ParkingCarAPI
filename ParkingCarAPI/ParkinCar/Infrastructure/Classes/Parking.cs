﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingCar.Infrastructure.Interfaces;
using ParkingCar.Model;
using System.Threading;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace ParkingCar.Infrastructure.Classes
{
    public class Parking : IParking, IGetTransaction
    {
        private static readonly Lazy<Parking> instance =
        new Lazy<Parking>(() => new Parking(), LazyThreadSafetyMode.ExecutionAndPublication);

        private IWriringToFile writeToFile;
        private List<Transaction> tranList = new List<Transaction>();
        private List<Car> carList;
        public static double balance;
        private Dictionary<CarType, int> price;
        private int timeOut = Settings.TimeOut;
        private string path;
        private readonly ObservableCollection<Transaction> list;
        public ObservableCollection<Transaction> List
        {
            get
            {
                return list;
            }
        }


        public Parking(IWriringToFile _writeToFile,
                       List<Car> _carList,
                       //List<Transaction> _tranList,
                       CounterTranHandler _counterTran,
                       Dictionary<CarType, int> _price,
                       int _timeOut,
                       string _path)
        {
            writeToFile = _writeToFile;
            carList = _carList;
            // tranList = _tranList;
            price = _price;
            path = _path;
            list = new ObservableCollection<Transaction>();

            _counterTran.CounterEventHandler += (Object sender, TransactEventHandler arg) =>
            {

                if (timeOut == (int)arg.SignalTime)
                {
                    foreach (Car c in carList)
                    {
                        var value = price.FirstOrDefault(i => i.Key == c.CType).Value;
                        c.Balance = c.Balance - value;
                        balance += value;
                        tranList.Add(new Transaction { IdCar = c.IdCar, DateTimeTran = DateTime.Now, WriteOffs = value });
                        list.Add(new Transaction { IdCar = c.IdCar, DateTimeTran = DateTime.Now, WriteOffs = value });
                    }
                    Console.WriteLine("It's a 3 sec{0}", balance);
                }
                else
                {
                  //  list.AddRange(tranList);
                    Console.WriteLine("It's a 30 sec  {0}", list.Sum(i=> i.WriteOffs));
                    writeToFile.PushToFile(tranList, path, balance);
                    list.Clear();
                    tranList.Clear();
                }
            };
                  
        }

        public static Parking Instance
        {
            get
            {
                return instance.Value;
            }
        }

        private Parking()
        {
        }
        public Task<List<Transaction>> GetTranForMinute(List<Transaction> list)
        {
            var task = Task<List<Transaction>>.Run(() =>
            {
                var ls = list.Where(i => i.DateTimeTran.Minute == DateTime.Now.Minute)
                      .Select(i => i).ToList();
                return ls;
            });
            return task;
        }
    }

    public static class MyExtensions
    {
        public static void AddRange<T>(this ObservableCollection<T> observableCollection, IEnumerable<T> rangeList)
        {
            foreach (T item in rangeList)
            {
                observableCollection.Add(item);
            }
        }
    }
}

