﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingCarAPI.Model;
using ParkingCar.Infrastructure;
using ParkingCar.Model;

namespace ParkingCarAPI.Controllers
{
    [Route("api/[controller]")]

    public class CarController : Controller
    {

        private IRepository<Car> repo;
     
        public CarController(IRepository<Car> _repo, PContext ctx)
        {
            repo = _repo;           
        }
       

        [HttpGet]
        public async Task<IEnumerable<Car>> Get()
        {          
            return await repo.RetrieveAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(string id)
        {
            var t =  await repo.RetrieveAllAsync(i=>i.IdCar == int.Parse(id));
            if (t == null)
            {
                return new NotFoundResult();
            }
            else
                return new ObjectResult(t);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Car car)
        {

            if (car == null)
            {
                return new BadRequestResult();
            }
            await repo.CreateAsync(car);

            //return CreatedAtRoute("Get", new { id = car.IdCar }, car);
            return Ok(car);

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var delete = await repo.DeleteAsync(int.Parse(id));

            if (delete == false)
            {
                return NotFound();
            }
            else
            {
                return new NoContentResult();
            }
        }
    }      
}
