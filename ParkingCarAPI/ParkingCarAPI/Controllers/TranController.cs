﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingCarAPI.Model;
using ParkingCar.Model;

namespace ParkingCarAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Tran")]
    public class TranController : Controller
    {
        private IRepository<Transaction> repo;
        public TranController(IRepository<Transaction> _repo)
        {
            repo = _repo;
        }
        [HttpGet]
        public async Task<IEnumerable<Transaction>> Get()
        {
            return await repo.RetrieveAllAsync();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(string id)
        {
            var t = await repo.RetrieveAllAsync(i => i.DateTimeTran.Minute == DateTime.Now.Minute);
            if (t == null)
            {
                return new NotFoundResult();
            }
            else
                return new ObjectResult(t);
        }

        [HttpGet("{m}/{id}")]
        public async Task<ActionResult> Get(string m, string id)
        {
            var t = await repo.RetrieveAllAsync(i => i.DateTimeTran.Minute == DateTime.Now.Minute
            && i.IdCar  == int.Parse(id)
            );
            if (t == null)
            {
                return new NotFoundResult();
            }
            else
                return new ObjectResult(t);
        }




    }
}