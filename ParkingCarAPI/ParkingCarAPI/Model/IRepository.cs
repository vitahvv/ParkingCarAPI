﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ParkingCarAPI.Model
{
    public interface IRepository<T> where T : class
    {
        //  List<T> Cars { get; set; }
          Task<IEnumerable<T>> RetrieveAllAsync();
          Task<IEnumerable<T>> RetrieveAllAsync(Expression<Func<T, bool>> predicate = null);
          Task<bool> CreateAsync(T t);
        Task<bool> CreateListAsync(IEnumerable<T> t);
          Task<bool> DeleteAsync(int id);

    }
}
