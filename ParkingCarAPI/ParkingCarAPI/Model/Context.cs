﻿using Microsoft.EntityFrameworkCore;
using ParkingCar.Model;


namespace ParkingCarAPI.Model
{
     public class PContext: DbContext
    {

        public PContext()
        {
           
        }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=dbparking");
        }

    }
           
}
