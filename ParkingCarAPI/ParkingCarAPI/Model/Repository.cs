﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ParkingCarAPI.Model
{
    public class Repository<T> : IRepository<T> where T : class
    {
        DbSet<T> dbSet;
        PContext ctx;
        public Repository(PContext _ctx)
        {
            this.ctx = _ctx;
            dbSet = ctx.Set<T>();          
        }

        public async Task<IEnumerable<T>> RetrieveAllAsync()
        {
            return await Task.Run<IEnumerable<T>>(
            () => dbSet.Select(i => i));
        }


        public async Task<IEnumerable<T>> RetrieveAllAsync(Expression<Func<T, bool>> predicate = null)
        {

            var result = dbSet.Where(predicate).ToListAsync();

            if (result != null)
            {
                return await result;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> CreateAsync(T t)
        {

            return await Task.Run(() =>
            {
                if (t != null)
                {
                    dbSet.Add(t);
                    ctx.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }

        public async Task<bool> CreateListAsync(IEnumerable<T> t)
        {

            return await Task.Run(() =>
            {
                if (t != null)
                {
                    dbSet.AddRange(t);
                    ctx.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            });
        }



        public async Task<bool> DeleteAsync(int id)
        {
            var result = dbSet.Find(id);

            return await Task.Run(() =>
             {
                 dbSet.Remove(result);
                 int affected = ctx.SaveChanges();
                 if (affected ==1)
                 {
                     

                     return true;
                 }
                 else
                 {
                     return false;
                 }
             });

        }
    }
}
