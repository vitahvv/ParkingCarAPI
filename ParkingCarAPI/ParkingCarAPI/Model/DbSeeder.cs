﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingCar.Model;

namespace ParkingCarAPI.Model
{
    public static class DbSeeder
    {

        static DbSeeder()
        {
            
        }

        public static async void Seed()
        {
            PContext DbContext = new PContext();
            DbContext.Database.EnsureDeleted();
            DbContext.Database.EnsureCreated();

            var items = new List<Car>
          {
            new Car {IdCar=1,CType=CarType.Truck, Balance=5000 },
            new Car { IdCar=2,CType=CarType.Cars, Balance=2000},
            new Car { IdCar=3,CType=CarType.Bus, Balance=3000},
            new Car { IdCar=4,CType=CarType.Truck, Balance=5000}
            };

            var tran = new List<Transaction>
          {
            new Transaction {IdCar=1,DateTimeTran= DateTime.Now },
            new Transaction {IdCar=2,DateTimeTran= DateTime.Now},
            
            };

            items.ForEach(s => DbContext.Cars.Add(s));
            tran.ForEach(s => DbContext.Transactions.Add(s));
            await DbContext.SaveChangesAsync();
        }
    }
}
