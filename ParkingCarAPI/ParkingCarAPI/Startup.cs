﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ParkingCarAPI.Model;
using ParkingCar.Model;
using ParkingCar.Infrastructure.Classes;
using ParkingCar.Infrastructure.Interfaces;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace ParkingCarAPI
{
    public class Startup
    {        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<PContext>(options =>
            //options.UseSqlServer(Configuration.GetConnectionString("dbparking")));
            services.AddSingleton<PContext>();
            //services.AddDbContext<PContext>(options => options.UseSqlite("Data Source=dbparking"));
            //services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IRepository<Car>, Repository<Car>>();
            services.AddScoped<IRepository<Transaction>, Repository<Transaction>>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // app.UseMiddleware<PContext>();
                app.UseDeveloperExceptionPage();
                // app.UseBrowserLink();
            }
            var c = new StartApp(new Repository<Car>(new PContext()), new Repository<Transaction>(new PContext()));
            c.PakingAPP();
            // DbSeeder.Seed();
            app.UseMvc();
        }
    }


    public  class StartApp
    {
        private  IRepository<Car> repo;
        private static int timeOut = Settings.TimeOut;
        private static string path = Settings.Path;
        private static Dictionary<CarType, int> price = Settings.priceOfParking;
        private static List<Car> cars;
        private  WriringToFile writeToFile;
        private static List<Transaction> tranList;
        private static CounterTranHandler tran;
        private  IRepository<Transaction> trepo;
        private  Parking p;
        private PContext ctx;
        private ObservableCollection<Transaction> ls;
        


        public StartApp(IRepository<Car> _repo, IRepository<Transaction> _trepo)
        {
            repo = _repo;
            trepo = _trepo;
            cars = new List<Car>(GetCar(repo).Result);          
            writeToFile = new WriringToFile(path, cars);            
            tran = new CounterTranHandler();
            ctx =  new PContext();
            p = new Parking(writeToFile, cars, tran, price, timeOut, path);

            p.List.CollectionChanged += (object sender, NotifyCollectionChangedEventArgs args) =>
            {
                ls = p.List;
                if (CreateTran(trepo, ls).Result == true)
                {
                    Console.WriteLine("Added data to database");
                }
                else
                {
                    Console.WriteLine(" have a problem");
                }
            };
        }
        
        public  void PakingAPP()
        {          
            Start(tran, true);                      
        }
                                 
        public static async Task<IEnumerable<Car>> GetCar(IRepository<Car> repo)
        {
            return await repo.RetrieveAllAsync();
        }

        public static async Task<bool> CreateTran(IRepository<Transaction> trepo, IEnumerable<Transaction> t)
        {
            await trepo.CreateListAsync(t);
            return true;

        }
        public static void Start(CounterTranHandler tran, bool enableTimer)
        {
            tran.WripperTime(3000, enableTimer);
            tran.WripperTime(30000, enableTimer);
        }
    }

}
