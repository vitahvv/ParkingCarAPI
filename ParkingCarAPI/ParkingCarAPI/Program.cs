﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ParkingCar.Model;
using ParkingCar.Infrastructure.Interfaces;
using ParkingCar.Infrastructure.Classes;
using ParkingCarAPI.Model;

namespace ParkingCarAPI
{
    public class Program
    {


        private static IRepository<Car> repo;
        private static int timeOut = Settings.TimeOut;
        private static string path = Settings.Path;
        private static Dictionary<CarType, int> price = Settings.priceOfParking;
        
        public Program(IRepository<Car> _repo)
        {
            repo = _repo;
        }
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();          

        }


        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
        
    }
}
